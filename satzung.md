# Satzung des Vereins Jugendzentrum Crailsheim e.V.

## §1 Name, Sitz und Zweck des Vereins

Der Verein Jugendzentrum Crailsheim e.V., mit Sitz in Crailsheim, verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts „Steuerbegünstigte Zwecke" der Abgabenordnung.

### Zweck des Vereins ist:

1. Die Trägerschaft, Organisation und Verwaltung des Jugendzentrums.
2. Die Interessen der Jugendlichen der Stadt Crailsheim und Umgebung, in Bezug auf das Jugendzentrum, gegenüber der Öffentlichkeit und gegenüber den Behörden zu vertreten.
3. Die Tätigkeit im Bereich der Jugendpflege und Jugendfürsorge. Gewährleistung einer Jugendarbeit entsprechend den gesetzlichen Vorgaben des Kinder- und Jugendhilfegesetzes.

### Der Satzungszweck wird verwirklicht insbesondere durch:

- Angebote der Jugendbildung und -begegnung,
- Arbeitswelt, schul- und berufsorientierte Jugendbildung,
- Jugendarbeit in den Bereichen Sport, Spiel und Geselligkeit,
- Jugendkulturarbeit,
- Kinder- und Jugendfreizeiten,
- geschlechtsspezifische Jugendarbeit,
- Maßnahmen der Jugendsozialarbeit und Jugendberufshilfe, insbesondere durch ein sozialpädagogisch betreutes Beschäftigungsangebot für schwer vermittelbare junge Arbeitslose.

Im Rahmen dieses Angebotes legt der Verein ein besonderes Gewicht auf die Erhaltung unserer Kulturlandschaft im Rahmen von Arbeiten zur Biotop- und Landschaftspflege und betreibt eine Holz- und Reparaturwerkstatt vor allem zur Unterhaltung der von der Stadt zur Verfügung gestellten Räumlichkeiten.

## §2 Gemeinnützigkeit

1. Der Verein Jugendzentrum ist selbstlos tätig; er verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.
2. Mittel des Vereins dürfen nur für die satzungsgemäßen Zwecke verwendet werden. Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins.
3. Es darf keine Person durch Ausgaben, die dem Zweck der Körperschaft fremd sind oder durch unverhältnismäßig hohe Vergütung begünstigt werden.

## §3 Mittel des Vereins

Die Mittel zur Erfüllung seiner Aufgaben erhält der Verein durch:

1. Die Mitgliedsbeiträge, deren Höhe durch die Mitgliederversammlung bestimmt wird.
2. Zuschüsse und Subventionen der öffentlichen Hand.
3. Erlöse aus Veranstaltungen und Zweckbetrieben.
4. Erträgnisse aus dem Vereinsvermögen.
5. Geld- und Sachspenden.

## §4 Geschäftsjahr

Geschäftsjahr ist das Kalenderjahr.

## §5 Mitgliedschaft

1. Jeder kann Mitglied werden. Die Aufnahme in den Verein ist vom Vorstand zu bestätigen.
2. Mit dem Beitritt ist ein Förderbeitrag von mindestens 5€ zu bezahlen. Dieser Beitrag ist bis zum 31. März eines jeden Kalenderjahres zu entrichten. Wird bis zu diesem Zeitpunkt der Mitgliedsbeitrag nicht entrichtet, so hat der Vorstand das Recht, das betreffende Mitglied durch schriftliche Erklärung auszuschließen.
3. Jedes Mitglied hat das aktive und passive Wahlrecht.
4. Der Austritt kann jederzeit ohne Begründung erfolgen. Die Austrittserklärung muss dem Vorstand mitgeteilt werden.
5. Der Vorstand kann Mitglieder ausschließen, wenn diese den Zielen des Vereins entgegenwirken. Über eine Beschwerde des/der Betroffenen entscheidet die Mitgliedervollversammlung.

## §6 Organe des Vereins

1. Mitgliedervollversammlung
2. Arbeitsausschüsse
3. Vorstand

## §7 Mitgliedervollversammlung

1. Sie umfasst alle eingetragenen Mitglieder.
2. Sie findet mindestens zweimal jährlich statt und jeweils 14 Tage nach einer Vollversammlung (siehe §13). Hierzu werden alle Mitglieder 5 Tage vorher mit Bekanntgabe der Tagesordnung schriftlich eingeladen.
3. Sie kann auf Antrag mit Begründung von mindestens 1/10 der Mitglieder oder auf Beschluss des Vorstandes einberufen werden. Einberufung der Mitgliedervollversammlung erfolgt 5 Tage vorher unter Angabe der Tagesordnung durch den Vorstand.

## §8 Aufgaben der Mitgliedervollversammlung

Die Mitgliedervollversammlung (MVV) hat folgende Aufgaben:

1. Die Wahl des Vorstandes auf ein Jahr.
2. Die Wahl von Kassenprüfern. Die Kassenprüfer haben das Recht, die Vereinskasse und die Buchführung jederzeit zu überprüfen. Eine Prüfung muss aber mindestens viermal im Jahr erfolgen. Über die Prüfung muss der Mitgliederversammlung (MV) Bericht erstattet werden.
3. Den Jahres- und Kassenbericht der Ausschüsse und des Vorstands, die Kassenprüfungsberichte der Kassenprüfer entgegenzunehmen und die Entlastung zu erteilen.
4. Den Haushaltsplan aufzustellen. Der Haushaltsplan wird vom Vorstand vorbereitet.
5. Beschlüsse zu fassen über Satzungsänderungen und alle anderen vom Vorstand unterbreiteten Aufgaben, sowie über die in der Satzung festgelegten Angelegenheiten.
6. Beschlussfassung über Anträge.

## §9 Beschlussfassung der Mitgliedervollversammlung

1. Den Vorsitz in der MVV führt ein vorher vom Vorstand bestimmtes Mitglied des Vorstands.
2. Die MVV fasst ihre Beschlüsse mit einfacher Stimmenmehrheit. Eine Vertretung in der Stimmabgabe ist nicht zulässig.
3. Die Wahl des Vorstands erfolgt auf Antrag geheim. Geheime Beschlussfassung ansonsten, sobald mindestens ein Mitglied den Antrag dazu stellt.

## §10 Arbeitsausschüsse

1. Arbeitsausschüsse bestehen aus Mitgliedern des Vereins, die sich in Ausschusslisten eintragen.
2. Die Arbeitsausschüsse wählen je einen Sprecher auf die Dauer eines Jahres. Wiederwahl ist möglich.
3. Die Ausschusssprecher nehmen an Vorstandssitzungen teil. Sie haben hierbei Rede- und Antragsrecht.

## §11 Vorstand

1. Der Vorstand besteht aus drei Mitgliedern, der/dem Vorsitzenden, der/dem Schriftführer/-in und dem/der Kassier/-in. (Der Vorstand vertritt den Verein gerichtlich und außergerichtlich im Sinne des §26 BGB). Jedes Vorstandsmitglied ist allein vertretungsberechtigt. Des weiteren werden zwei bis sechs Beisitzer/-innen mit beratender Funktion von der MVV gewählt.
2. Der Vorstand führt die laufenden Geschäfte des Vereins, verwaltet das Vereinsvermögen und sorgt für Ausführung der Beschlüsse der MV.
3. Der/die Kassier/-in verwaltet das Vereinsvermögen.
4. Der Vorstand wird von der MV auf die Dauer eines Jahres gewählt. Er bleibt solange im Amt, bis ein neuer Vorstand gewählt ist. Die Wiederwahl ist möglich.

## §12 Beschlussfassung des Vorstands

1. Der Vorstand fasst seine Beschlüsse in Vorstandssitzungen, die vom Vorsitzenden einberufen werden. Der Vorstand ist beschlussfähig, wenn alle Vorstandsmitglieder anwesend sind. Bei Beschlussunfähigkeit muss der Vorsitzende binnen drei Tagen eine zweite Sitzung mit derselben Tagesordnung einberufen. Diese ist beschlussfähig, wenn drei Mitglieder des Vorstands erschienen sind. In der Einladung zur zweiten Sitzung ist auf die besondere Beschlussfähigkeit hinzuweisen.
2. Der Vorstand fasst die Beschlüsse mit einfacher Mehrheit der erschienenen Mitglieder.

## §13 Vollversammlung der Jugendhausbesucher

1. In Fragen, die die Arbeit im Jugendzentrum betreffen, wird jährlich mindestens einmal eine Vollversammlung einberufen.
2. Die Vollversammlung setzt sich zusammen aus alles Besuchern des Jugendzentrums und den Mitgliedern des Vereins.
3. Die Vollversammlung hat Vorschlagsrecht in allen das Jugendzentrum betreffenden Bereichen.

## §14 Beurkundung von Beschlüssen , Niederschriften

1. Beschlüsse des Vorstands und der MV sind schriftlich abzufassen und dem Vorsitzenden zur Unterschrift vorzulegen.
2. Über jede Vollversammlung wird eine Niederschrift aufgenommen, die zu veröffentlichen ist.

## §15 Satzungsänderungen

Die Satzung kann durch Beschluss der MV geändert werden. Bei der Einladung ist der zu ändernde Paragraph der Satzung in der Tagesordnung anzugeben. Ein Beschluss, der die Änderung der Satzung beinhaltet, bedarf der Mehrheit von zwei Dritteln der erschienenen Mitglieder.

## §16 Vereinsauflösung

1. Die Auflösung des Vereins erfolgt durch Beschluss der Mitgliederversammlung, wobei drei Viertel der erschienenen Mitglieder für die Auflösung stimmen müssen.
2. Die MV ernennt zur Abwicklung der Geschäfte drei Liquidatoren.
3. Im Falle der Auflösung des Vereins oder bei Wegfall steuerbegünstigter Zwecke wird nach Abzug sämtlicher Verbindlichkeiten das verbleibende Vereinsvermögen auf eine steuerbegünstigte oder öffentlich-rechtliche Einrichtung übertragen und dieses für gemeinnützige, der offenen Kinder- und Jugendarbeit dienende Zwecke verwendet.

Geänderte Fassung vom 29. September 2014
